/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw.runnableproject;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.daw.runnableproject.runnableclasses.*;

/**
 *
 * @author rgcenteno
 */
public class RunnableProject {

    private static int max = 10;
    
    public static void main(String[] args) throws InterruptedException {
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        int tamano = 45;
        String input = "";
        do{
            System.out.println(com.google.common.base.Strings.padEnd("*", tamano, '*'));
            System.out.println(com.google.common.base.Strings.padEnd("* 1. Hilo contador estilo clásico", tamano-1, ' ') + "*");
            System.out.println(com.google.common.base.Strings.padEnd("* 2. Hilo variable común estilo clásico", tamano-1, ' ') + "*");
            System.out.println(com.google.common.base.Strings.padEnd("* 3. Hilo contador con fixed pool", tamano-1, ' ') + "*");
            System.out.println(com.google.common.base.Strings.padEnd("* 4. Hilo variable común fixed pool", tamano-1, ' ') + "*");
            System.out.println(com.google.common.base.Strings.padEnd("* ", tamano-1, ' ') + "*");
            System.out.println(com.google.common.base.Strings.padEnd("* 0. Salir", tamano-1, ' ') + "*");
            System.out.println(com.google.common.base.Strings.padEnd("*", tamano, '*'));
            input = teclado.nextLine();
            switch(input){
                case "1":
                    test1();
                    break;
                case "2":
                    test2();
                    break;
                case "3":
                    test11();
                    break;
                case "4":
                    test12();
                    break;
                case "0":                
                    break;
                default:
                    System.out.println("Opción inválida");
                    break;
            }
        }
        while(!input.equals("0"));
    }
    
    private static void test1() throws InterruptedException{
        Thread[] hilos = new Thread[max];
        for (int i = 0; i < hilos.length; i++) {
            hilos[i] = new Thread(new RunnableTest(i+1));
            hilos[i].start();
            
        }
        for (int i = 0; i < hilos.length; i++) {
            hilos[i].join();
        }
        System.out.println("Todos finalizados");
    }
    
    private static void test2() throws InterruptedException{
        ClaseContenedora c = new ClaseContenedora();
        Thread[] hilos = new Thread[max];
        for (int i = 0; i < hilos.length; i++) {
            hilos[i] = new Thread(new RunnableCampoComun(i+1, c));
            hilos[i].start();
            
        }
        for (int i = 0; i < hilos.length; i++) {
            hilos[i].join();
        }
        System.out.println("Todos finalizados " + c.getContador());
    }
    
    private static void test11(){
        try {            
            ExecutorService executor = Executors.newFixedThreadPool(5);
            for (int i = 0; i < max; i++) {                        
                executor.execute(new RunnableTest(i+1));
            }
            //No permitimos que añadan más elementos
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);
            System.out.println("Todos finalizados ");
        } catch (InterruptedException ex) {
            Logger.getLogger(RunnableProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void test12(){
        try {
            ClaseContenedora c = new ClaseContenedora();
            ExecutorService executor = Executors.newFixedThreadPool(5);
            for (int i = 0; i < max; i++) {                        
                executor.execute(new RunnableCampoComun(i+1, c));
            }
            //No permitimos que añadan más elementos
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);
            System.out.println("Todos finalizados " + c.getContador());
        } catch (InterruptedException ex) {
            Logger.getLogger(RunnableProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
