/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.daw.runnableproject.runnableclasses;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael González Centeno
 */
public class RunnableTest implements Runnable{

    private final int id;
    private int ejecuciones;
    private java.util.Random random;
    
    private static final int MAX = 10;

    public RunnableTest(int id) {
        this.id = id;
        ejecuciones = 0;
        random = new java.util.Random();
    }
    
    
    @Override
    public void run() {
        do{
            ejecuciones++;
            System.out.println("Soy el hilo " + id + ". Ejecución: " + ejecuciones);
            try {
                Thread.sleep(random.nextInt(5000));
            } catch (InterruptedException ex) {
                Logger.getLogger(RunnableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        while(ejecuciones < MAX);
        System.out.println("FINAL HILO: " + id);
    }
    
}
